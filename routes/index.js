const express = require('express');
const router = express.Router({strict: true});
const lib = require('../lib/main')
const gdatastore = require('../lib/model-datastore')

/* GET home page. [General version] */
router.get('/', function(req, res, next) {
  const switchNum = lib.getRandom(0, 1)
  let redirectUrl = '/home/normal/feature'

  switch (switchNum){
      case 0:
          redirectUrl = '/home/normal/feature'
          break;
      default:
          redirectUrl = '/home/normal/makeup'
          break;
  }
  res.redirect(redirectUrl)
});

/* GET home page. [Business version] */
router.get('/business', function(req, res, next) {
    const switchNum = lib.getRandom(0, 1)
    let redirectUrl = '/home/feature'

    switch (switchNum){
        case 0:
            redirectUrl = '/home/business/feature'
            break;
        default:
            redirectUrl = '/home/business/makeup'
            break;
    }
    res.redirect(redirectUrl)
});

/**
 * version:
 * 1. normal
 * 2. business
 * mode:
 * 1. feature
 * 2. makeup
 * */
router.get('/home/:version/:mode', function(req, res, next) {
    const mode = req.params.mode
    const version = req.params.version
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();
    const maxCarouselNum = 5;

    gdatastore.list('Page_Caption', 10, 'page_name', req.query.pageToken, (err, entities, cursor) => {
        if (err){
            console.error('[gdatastore err] ', err)
            next(err);
            return;
        }
        let captionList = {}
        entities.some(function (list, i, array) {
            if (list.page_name === 'index'){
                captionList = list
                return true;
            }
        })

        res.render('index', { title: mode, version: version, nextPage: `/select/${version}/${mode}`,  captionList: captionList, carousels: lib.getCarouselImgArray(maxCarouselNum)});
    })
});

module.exports = router;
