const express = require('express')
const router = express.Router({strict: true})
const lib = require('../lib/main')
const gdatastore = require('../lib/model-datastore')
const sconfig = require('../lib/webhook_config.json')

/* GET select page.*/
/**
 * version:
 * 1. normal
 * 2. business
 * mode:
 * 1. feature
 * 2. makeup
 * */
router.get('/:version/:mode/topic', (req, res, next) => {
    console.log(req.query.param)
    const mode = req.params.mode
    const version = req.params.version
    console.log(mode)
    console.log(version)

    let serial_number = req.query.sNum ? req.query.sNum : "none"
    const evaTypes = req.query.evaType.split(',')
    const bType = req.query.bType ? req.query.bType : "b1"
    const hType = req.query.hType ? req.query.hType : "h1"
    const tType = req.query.tType ? req.query.tType : "t1"
    const mtType = req.query.mtType ? req.query.mtType : "mt1"
    const productObj = lib.getProduct(bType, hType, mtType)
    let currentObj = {
        bType: bType,
        hType: hType,
        tType: tType,
        mtType: mtType,
        productImg: productObj.imgUrl,
        productFileName: productObj.fileName,
        serial_number: serial_number,
        surveycake_url: `${sconfig.Surveycake.normal.url}?ssn99=${this.serial_number}`
    }
    let guestObj = {
        isReplySurveycake: false,
        productFileName: currentObj.productFileName,
        selected_brush: currentObj.bType,
        selected_handle: currentObj.hType,
        selected_material: currentObj.mtType,
        version: version,
        mode: mode
    }
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();

    gdatastore.list('Page_Caption', 10, 'page_name', req.query.pageToken, (err, entities, cursor) => {
        if (err){
            console.error('[gdatastore err] ', err)
            next(err);
            return;
        }
        let captionList = {}
        entities.some(function (list, i, array) {
            if (list.page_name === 'complete_topic'){
                captionList = list
                return true;
            }
        })

        if (serial_number === 'none'){
            lib.getSerialNumber(guestObj, serial_num =>{
                let sUrl = ''
                switch (version) {
                    case "normal":
                        sUrl = sconfig.Surveycake.normal.url
                        break;
                    case "business":
                        sUrl = sconfig.Surveycake.business.url
                        break;
                }

                currentObj.serial_number = serial_num
                currentObj.surveycake_url = `${sUrl}?ssn99=${currentObj.serial_number}`
                res.render('complete_topic', { title: 'topic', version: version, mode: mode,  captionList: captionList, currentObj: currentObj});
            })
            return false;
        }

        res.render('complete_topic', { title: 'topic', version: version, mode: mode,  captionList: captionList, currentObj: currentObj});
    })
})

router.get('/:version/:mode/thanks', (req, res, next) => {
    const mode = req.params.mode
    const version = req.params.version
    const productFileName = req.query.product ? req.query.product : "B1_M01H01"
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();

    // Select index page from version.
    const indexPage = version === 'business' ? '/business' : '/'

    const maxPuzzleImgNum = 11
    let puzzleImgList = lib.getPuzzleImgArray(maxPuzzleImgNum)
    puzzleImgList.splice(5, 0, lib.getProductUrl(productFileName))  // Animation img at index 5.

    gdatastore.list('Page_Caption', 10, 'page_name', req.query.pageToken, (err, entities, cursor) => {
        if (err){
            console.error('[gdatastore err] ', err)
            next(err);
            return;
        }
        let captionList = {}
        entities.some(function (list, i, array) {
            if (list.page_name === 'complete_thanks'){
                captionList = list
                return true;
            }
        })

        res.render('complete_thanks', { title: 'thanks', version: version, mode: mode, indexPage: indexPage, captionList: captionList, puzzleImgList: puzzleImgList})
    })
})

module.exports = router