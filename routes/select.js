const express = require('express')
const router = express.Router({strict: true})
const lib = require('../lib/main')
const gdatastore = require('../lib/model-datastore')

/* GET select page.*/
/**
 * version:
 * 1. normal
 * 2. business
 * mode:
 * 1. feature
 * 2. makeup
 * */
router.get('/:version/:mode', (req, res, next) => {
    const mode = req.params.mode
    const version = req.params.version
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();
    res.render(`select_${mode}`, { title: mode, version: version})
})

router.get('/:version/:mode/brush', (req, res, next) => {
    console.log(req.query.evaType)
    console.log(req.query.bType)
    const evaTypes = req.query.evaType.split(',')
    const bType = req.query.bType ? req.query.bType : "b1"
    let displayImg = 'none'
    let currentObj = {
        bType: bType,
        evaTypes: evaTypes,
        displayImg: displayImg,
        description: '',
        title: '',
        eva_feature: [],
        eva_makeup: []
    }
    const mode = req.params.mode
    const version = req.params.version
    console.log(mode)
    console.log(version)
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();

    gdatastore.list('Brush', 10, 'code', req.query.pageToken, (err, entities, cursor) =>{
        if (err){
            console.error('[gdatastore err] ', err)
            next(err);
            return;
        }

        currentObj.displayImg = entities[0].img_url
        if (bType){
            entities.some(function (item, i, array) {
                if (bType === item.code){
                    currentObj.displayImg = item.img_url
                    currentObj.description = item.description
                    currentObj.title = item.title
                    if (evaTypes[0].indexOf('f') >= 0){
                        evaTypes.forEach(function (f, i, array) {
                            currentObj.eva_feature.push({
                                typeName: f,
                                value: item.eva_feature[f]
                            })
                        })
                    }
                    if (evaTypes[0].indexOf('m') >= 0){
                        evaTypes.forEach(function (m, i, array) {
                            currentObj.eva_makeup.push({
                                typeName: m,
                                value: item.eva_makeup[m]
                            })
                        })
                    }
                    return true;
                }
            })
        }
        res.render('select_brush', { title: 'brush', version: version, mode: mode, brushList: entities, currentObj: currentObj})
    })
})

router.get('/:version/:mode/trait', (req, res, next) => {
    const mode = req.params.mode
    const version = req.params.version
    const evaTypes = req.query.evaType
    const bType = req.query.bType ? req.query.bType : "b1"
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();
    if (mode === 'makeup'){
        return res.redirect(`/select/${version}/${mode}/handle?evaType=${evaTypes}&bType=${bType}`)
    }
    res.render('select_trait', { title: 'trait', version: version, mode: mode})
})

router.get('/:version/:mode/handle', (req, res, next) => {
    const evaTypes = req.query.evaType.split(',')
    const bType = req.query.bType ? req.query.bType : "b1"
    const hType = req.query.hType ? req.query.hType : "h1"
    const tType = req.query.tType ? req.query.tType : "t1"
    let displayImg = 'none'
    let currentObj = {
        bType: bType,
        hType: hType,
        tType: tType,
        displayImg: displayImg,
        displayImgBrush: '',
        description: '',
        title: '',
        eva_trait_num: 0,
        eva_brush_people: {}
    }
    const mode = req.params.mode
    const version = req.params.version
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();

    gdatastore.list('Handle', 12, 'number', req.query.pageToken, (err, entities, cursor) =>{
        if (err){
            console.error('[gdatastore err] ', err)
            next(err);
            return;
        }

        currentObj.displayImg = entities[0].img_url
        if (hType){
            entities.some(function (item, i, array) {
                if (hType === item.code){
                    currentObj.displayImg = item.img_url
                    currentObj.description = item.description
                    currentObj.title = item.title
                    currentObj.eva_trait_num = item.eva_trait[currentObj.tType]
                    currentObj.eva_brush_people["name"] = item.eva_brush_people[currentObj.bType].name
                    currentObj.eva_brush_people["num"] = item.eva_brush_people[currentObj.bType].num
                    currentObj.displayImgBrush = item.img_brush_list[currentObj.bType].url
                    return true;
                }
            })
        }

        res.render('select_handle', { title: 'handle', version: version, mode: mode, handleList: entities, currentObj: currentObj})
    })
})

router.get('/:version/:mode/material', (req, res, next) => {
    const mode = req.params.mode
    const version = req.params.version
    const evaTypes = req.query.evaType.split(',')
    const bType = req.query.bType ? req.query.bType : "b1"
    const hType = req.query.hType ? req.query.hType : "h1"
    const tType = req.query.tType ? req.query.tType : "t1"
    const mtType = req.query.mtType ? req.query.mtType : "mt1"
    let displayImg = 'none'
    let currentObj = {
        bType: bType,
        hType: hType,
        tType: tType,
        mtType: mtType,
        displayImg: displayImg,
        displayImgBrush: '',
        displayImgBg: '',
        description: '',
        title: ''
    }
    if (mode !== 'feature' && mode !== 'makeup') return res.status(404).end();
    gdatastore.list('Material', 18, 'number', req.query.pageToken, (err, entities, cursor) =>{
        if (err){
            console.error('[gdatastore err] ', err)
            next(err);
            return;
        }

        if (mtType){
            entities.some(function (item, i, array) {
                if (mtType === item.code){
                    currentObj.description = item.description
                    currentObj.title = item.title
                    currentObj.displayImg = item.img_handle_brush[currentObj.hType].url
                    currentObj.displayImgBrush = item.img_handle_brush[currentObj.hType].brush_list[currentObj.bType]
                    currentObj.displayImgBg = item.imgBg_url
                    return true;
                }
            })
        }

        res.render('select_material', { title: 'material', version: version, mode: mode, materialList: entities, currentObj: currentObj})
    })
})

module.exports = router