const express = require('express')
const router = express.Router({strict: true})
const request = require('request')
const lib = require('../lib/main')
const gdatastore = require('../lib/model-datastore')
const decode = require('../lib/webhook_config.json')

router.post('/normal', (req, res, next) => {
    const svid = req.body.svid
    const hash = req.body.hash
    console.error('[Webhook] body: ',req.body)
    console.error('[Webhook] svid: ',svid)
    console.error('[Webhook] hash: ',hash)

    request({
        uri: lib.getSurveycakeRequestUrl(svid, hash),
        method: "POST"
    }, (err, res, body)=>{
        console.error('[Surveycake callback] err: ', err)

        let deObj = lib.decrypt(decode.Surveycake.normal.hash, decode.Surveycake.normal.IV, body)
        deObj = JSON.parse(deObj)

        let serial_number = lib.getSurveycakeSerialNumber(deObj.result)
        console.error('[serial_number]: ', serial_number)

        const kind = 'Guest'
        gdatastore.read(kind, serial_number, (err, data) =>{
            if (err){
                return console.error(`[${err.code}]: ${err.message}`)
            }

            data.isReplySurveycake = true
            delete data.id
            gdatastore.update_Guest(serial_number, data, (err, data) => {
                if (err){
                    return conosle.error(`[${serial_number}]: ${err}`)
                }
                console.error(`[${serial_number}]: Reply complete.`)
            })
        })
    })

    res.status(200).end()
})

router.post('/business', (req, res, next) => {
    const svid = req.body.svid
    const hash = req.body.hash

    request({
        uri: lib.getSurveycakeRequestUrl(svid, hash),
        method: "POST"
    }, (err, res, body)=>{
        console.error('[Surveycake callback] err: ', err)

        let deObj = lib.decrypt(decode.Surveycake.business.hash, decode.Surveycake.business.IV, body)
        deObj = JSON.parse(deObj)

        let serial_number = lib.getSurveycakeSerialNumber(deObj.result)
        console.error('[serial_number]: ', serial_number)

        const kind = 'Guest'
        gdatastore.read(kind, serial_number, (err, data) =>{
            if (err){
                return conosle.error(`[${err.code}]: ${err.message}`)
            }

            data.isReplySurveycake = true
            delete data.id
            gdatastore.update_Guest(serial_number, data, (err, data) => {
                if (err){
                    return conosle.error(`[${serial_number}]: ${err}`)
                }
                console.error(`[${serial_number}]: Reply complete.`)
            })
        })
    })

    res.status(200).end()
})

module.exports = router