const express = require('express')
const router = express.Router({strict: true})
const lib = require('../lib/main')
const gdatastore = require('../lib/model-datastore')

let resObj = {}

router.post('/GetBrushList', (req, res, next) => {
    gdatastore.list('Brush', 10, 'code', req.query.pageToken, (err, entities, cursor) =>{
        if (err){
            console.error('[gdatastore err] ', err.toString())
            next(err);
            resObj.errCode = 999
            resObj.errorMsg = err
            res.json(resObj)
            return;
        }
        resObj.errCode = 0
        resObj.content = entities
        res.json(resObj)
    })
})

router.post('/GetHandleList', (req, res, next) => {
    gdatastore.list('Handle', 12, 'number', req.query.pageToken, (err, entities, cursor) =>{
        if (err){
            console.error('[gdatastore err] ', err.toString())
            next(err);
            resObj.errCode = 999
            resObj.errorMsg = err
            res.json(resObj)
            return;
        }
        resObj.errCode = 0
        resObj.content = entities
        res.json(resObj)
    })
})

router.post('/GetMaterialList', (req, res, next) => {
    gdatastore.list('Material', 18, 'number', req.query.pageToken, (err, entities, cursor) =>{
        if (err){
            console.error('[gdatastore err] ', err.toString())
            next(err);
            resObj.errCode = 999
            resObj.errorMsg = err
            res.json(resObj)
            return;
        }
        resObj.errCode = 0
        resObj.content = entities
        res.json(resObj)
    })
})

router.post('/CheckSurveycakeEnd', (req, res, next) => {
    const serial_number = req.body.serial_number
    console.error('[CheckSurveycakeEnd] serial_number: ',serial_number)

    gdatastore.read('Guest', serial_number, (err, data) =>{
        if (err){
            console.error('[gdatastore err] ', err.toString())
            next(err);
            resObj.errCode = 999
            resObj.errorMsg = err
            res.json(resObj)
            return;
        }
        resObj.errCode = 0
        resObj.isReplySurveycake = data.isReplySurveycake
        res.json(resObj)
    })
})

module.exports = router