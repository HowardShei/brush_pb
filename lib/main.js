const crypto = require('crypto')
const gdatastore = require('./model-datastore')
const sconfig = require('./webhook_config.json')

/***
 *  加密方法
 * @param key       金鑰
 * @param iv          向量
 * @param data      加密數據
 * @returns {Buffer|string}
 */
function encrypt(key, iv, data) {
    let cipher = crypto.createCipher('aes-128-cbc', key, iv)
    let crypted = cipher.update(data, 'utf8', 'binary')
    crypted += cipher.final('binary')
    crypted = Buffer.from(crypted, 'binary').toString('base64')
    return crypted
}

/**
 *  解密方法
 * @param key           金鑰
 * @param iv              向量
 * @param crypted      密文
 * @returns {Buffer|string}
 */
function decrypt(key, iv, crypted) {
    crypted = Buffer.from(crypted, 'base64').toString('binary')
    let decipher = crypto.createDecipheriv('aes-128-cbc', key, iv)
    let decode = decipher.update(crypted, 'binary', 'utf8')
    decode += decipher.final('utf8')
    return decode
}

function getRandom(minNum, maxNum) {
    return Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum
}

function getPublicUrl(dir, filename) {
    // return `https://storage.cloud.google.com/brush-by-you.appspot.com/${dir}/${filename}`
    return `https://storage.googleapis.com/brush-by-you.appspot.com/${dir}/${filename}`
}

function getProduct(bType, hType, mtType) {
    let brushNum = bType.toUpperCase()
    let handleNum = hType.toUpperCase()
    let materialNum = mtType.replace('t', "").toUpperCase()
    let hChars = handleNum.split("")
    let mtChars = materialNum.split("")

    if (hChars.length <= 2){
        handleNum = `${hChars[0]}0${hChars[1]}`
    }
    if (mtChars.length <= 2){
        materialNum = `${mtChars[0]}0${mtChars[1]}`
    }
    let fileName = `${brushNum}_${materialNum}${handleNum}`

    return {
        fileName: fileName,
        imgUrl: `https://storage.googleapis.com/brush-by-you.appspot.com/product/${fileName}.png`
    }
}
function getProductUrl(productFileName) {
    return `https://storage.googleapis.com/brush-by-you.appspot.com/product/${productFileName}.png`
}

function getRandomProductFilename() {
    let carousel_B = getRandom(1, 7)
    let carousel_M = getRandom(1, 18)
    let carousel_H = getRandom(1, 12)

    carousel_M = carousel_M < 10 ? `0${carousel_M}` : carousel_M
    carousel_H = carousel_H < 10 ? `0${carousel_H}` : carousel_H
    return `B${carousel_B}_M${carousel_M}H${carousel_H}.png`
}

function getCarouselImgArray(count) {
    let carouselArray = []

    while (carouselArray.length < count){
        let productFileUrl = getPublicUrl('product', getRandomProductFilename())

        let isUrlExist = carouselArray.some(function (item, index, arary) {
            return productFileUrl === item
        })

        if (!isUrlExist){
            carouselArray.push(productFileUrl)
        }
    }
    return carouselArray
}

function getPuzzleImgArray(count) {
    let puzzleArray = []

    while (puzzleArray.length < count){
        let productFileUrl = getPublicUrl('product_mini', getRandomProductFilename())

        let isUrlExist = puzzleArray.some(function (item, index, arary) {
            return productFileUrl === item
        })

        if (!isUrlExist){
            puzzleArray.push(productFileUrl)
        }
    }
    return puzzleArray
}

function getSurveycakeRequestUrl(svid, hash) {
    const VERSION = 'v0'
    return `https://www.surveycake.com/webhook/${VERSION}/${svid}/${hash}`
}

/**
 * @param deObj     Decode object from surveycake.
 */
function getSurveycakeSerialNumber(deObj) {
    const sn = 99   // Surveycake default variable number.
    let sNum = ''
    deObj.some(function (item, i, array) {
        if (item.sn === sn){
            sNum = item.answer[0]
            return true;
        }
    })
    return parseInt(sNum)
}

function getSerialNumber(guestObj, callback) {
    gdatastore.create_Guest(guestObj, (err, data) =>{
        console.error(err)
        callback(data.id)
    })
}

module.exports = {
    getRandom,
    getPublicUrl,
    getCarouselImgArray,
    getPuzzleImgArray,
    getProduct,
    getProductUrl,
    getSurveycakeRequestUrl,
    encrypt,
    decrypt,
    getSurveycakeSerialNumber,
    getSerialNumber
}