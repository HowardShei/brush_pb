// Copyright 2017, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

'use strict';

const {Datastore} = require('@google-cloud/datastore');

// [START config]
const ds = new Datastore();
// const kind = 'Book';
// [END config]

// Translates from Datastore's entity format to
// the format expected by the application.
//
// Datastore format:
//   {
//     key: [kind, id],
//     data: {
//       property: value
//     }
//   }
//
// Application format:
//   {
//     id: id,
//     property: value
//   }
function fromDatastore(obj) {
    obj.id = obj[Datastore.KEY].id;
    return obj;
}

// Translates from the application's format to the datastore's
// extended entity property format. It also handles marking any
// specified properties as non-indexed. Does not translate the key.
//
// Application format:
//   {
//     id: id,
//     property: value,
//     unindexedProperty: value
//   }
//
// Datastore extended format:
//   [
//     {
//       name: property,
//       value: value
//     },
//     {
//       name: unindexedProperty,
//       value: value,
//       excludeFromIndexes: true
//     }
//   ]
function toDatastore(obj, nonIndexed) {
    nonIndexed = nonIndexed || [];
    const results = [];
    Object.keys(obj).forEach(k => {
        if (obj[k] === undefined) {
            return;
        }
        results.push({
            name: k,
            value: obj[k],
            excludeFromIndexes: nonIndexed.indexOf(k) !== -1,
        });
    });
    return results;
}

// Lists all books in the Datastore sorted alphabetically by title.
// The ``limit`` argument determines the maximum amount of results to
// return per page. The ``token`` argument allows requesting additional
// pages. The callback is invoked with ``(err, books, nextPageToken)``.
// [START list]
function list(kind, limit, order, token, cb) {
    const q = ds
        .createQuery([kind])
        .limit(limit)
        .order(order)
        .start(token);

    ds.runQuery(q, (err, entities, nextQuery) => {
        if (err) {
            cb(err);
            return;
        }
        const hasMore =
            nextQuery.moreResults !== Datastore.NO_MORE_RESULTS
                ? nextQuery.endCursor
                : false;
        cb(null, entities.map(fromDatastore), hasMore);
    });
}
// [END list]

// Creates a new book or updates an existing book with new data. The provided
// data is automatically translated into Datastore format. The book will be
// queued for background processing.
// [START update]
function update(kind, id, data, cb) {
    let key;
    let keyObj = {};
    if (id) {
        keyObj.path = [kind, parseInt(id, 10)];
    } else {
        keyObj.path = [kind];
    }
    key = ds.key(keyObj);

    const entity = {
        key: key,
        data: toDatastore(data, ['description']),
    };

    ds.save(entity, err => {
        data.id = entity.key.id;
        cb(err, err ? null : data);
    });
}
// [END update]

function update_Guest(id, data, cb) {
    const kind = 'Guest';
    let key;
    let keyObj = {};
    if (id) {
        keyObj.path = [kind, parseInt(id, 10)];
    } else {
        keyObj.path = [kind];
    }
    key = ds.key(keyObj);

    const entity = {
        key: key,
        data: toDatastore(data),
    };

    ds.save(entity, err => {
        data.id = entity.key.id;
        cb(err, err ? null : data);
    });
}

function create(kind, data, cb) {
    update(kind, null, data, cb);
}

function create_Guest(data, cb) {
    update_Guest(null, data, cb);
}

function read(kind, id, cb) {
    let key;
    let keyObj = {};
    keyObj.path = [kind, parseInt(id, 10)];
    key = ds.key(keyObj)

    ds.get(key, (err, entity) => {
        if (!err && !entity) {
            err = {
                code: 404,
                message: 'Not found',
            };
        }
        if (err) {
            cb(err);
            return;
        }
        cb(null, fromDatastore(entity));
    });
}

function _delete(kind, id, cb) {
    let key;
    let keyObj = {};

    keyObj.path = [kind, parseInt(id, 10)];
    key = ds.key(keyObj)

    ds.delete(key, cb);
}

// [START exports]
module.exports = {
    create,
    read,
    update,
    delete: _delete,
    list,
    update_Guest,
    create_Guest
};
// [END exports]
