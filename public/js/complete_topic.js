const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)

$(() => {

    const $prevBtn = $('#previous')

    $prevBtn.click(function () {
        if (pathObj.hasObj){
            const sNum = $(this).attr('data-sNum')
            const mtType = pathObj.param.mtType.toString()
            const hType = pathObj.param.hType.toString()
            const bType = pathObj.param.bType.toString()
            const evaType = pathObj.param.evaType.toString()
            let search = `evaType=${evaType}&bType=${bType}&hType=${hType}&mtType=${mtType}&sNum=${sNum}`
            if (pathObj.param.tType){
                search = search + `&tType=${pathObj.param.tType.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/material?${search}`
        }
    })

    $('#previousPhone').click(function () {
        $prevBtn.trigger('click')
    })

    let waitTime = 100    // Unit is seconds. Before start to check the param. // Business: 300s || Normal: 270s.
    if (pathObj.version === 'normal') waitTime = 270
    if (pathObj.version === 'business') waitTime = 300
    let loopCheck   // Time loop use in checking the value.
    const checkPerSeconds = 5
    const checkCountMax = 20
    let count = 0

    // console.log('waitTime: ', waitTime)

    function LoopCheck() {
        loopCheck = setInterval(function () {
            if (count === checkCountMax){
                clearInterval(loopCheck)
                setTimeout(function () {
                    LoopCheck()
                }, waitTime * 1000)
            }
            WebAPI.CheckSurveycakeEnd($prevBtn.attr('data-sNum'), res => {
                if (res.errCode === 0){
                    if (res.isReplySurveycake){
                        if (pathObj.hasObj){
                            clearInterval(loopCheck)
                            const productFileName = $('#completeImg').attr('data-fileName')
                            window.location.href = `/complete/${pathObj.version}/${pathObj.mode}/thanks?product=${productFileName}`
                        }
                    }
                }
            })
            count++
        }, checkPerSeconds * 1000)
    }

    setTimeout(function () {
        LoopCheck()
    }, waitTime * 1000)
})