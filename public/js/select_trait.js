const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)

$(() => {
    let selectBox = []
    const maxSelectNum = 1

    // Set the select-item selected if search exist.
    if (pathObj.param.hasOwnProperty('tType')){
        pathObj.param.tType.forEach(function (type, i, array) {
            $('.select-container').find('.select-item').each(function (i, item) {
                if ($(item).attr('data-num') === type){
                    $(item).addClass('selected')
                    selectBox.push(type)
                }
            })
        })
    }

    $('.select-item').click(function () {
        const itemNum = $(this).attr('data-num')
        let selectIndex = 0 // If isSelect is true, it can be used to store index from array.
        // console.log('itemNum: ', itemNum)

        // Check new item exist in the array.
        let isSelected = selectBox.some(function (item, index, array) {
            selectIndex = index
            return itemNum === item
        })

        if (isSelected){
            // To auto cancel selected item.
            $(this).removeClass('selected')
            selectBox.splice(selectIndex, 1)
        } else {
            // To auto select new item, and cancel first selected item.
            if (selectBox.length >= maxSelectNum){
                const firstItemNum = selectBox[0]
                $(`.select-item[data-num="${firstItemNum}"]`).removeClass('selected')
                selectBox.splice(0, 1)
            }

            $(this).addClass('selected')
            selectBox.push(itemNum)
        }

        // Use to portrait version.
        if (selectBox.length > 0) {
            $('.block-bottom').addClass('active')
        } else {
            $('.block-bottom').removeClass('active')
        }
    })

    $('#next').click(function () {
        if (selectBox.length < maxSelectNum){
            errMsgDialog.editInfo('你需要選擇一個刷柄的特色')
            errMsgDialog.showDialog()
            return false;
        }
        if (pathObj.hasObj){
            // console.log(selectBox.toString())
            const selectedParam = selectBox.toString()
            let currentSearch = `evaType=${pathObj.param.evaType.toString()}&bType=${pathObj.param.bType.toString()}&tType=${selectedParam}`

            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                currentSearch = currentSearch + `&sNum=${pathObj.param.sNum.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/handle?${currentSearch}`
        }
    })

    $('#previous').click(function () {
        if (pathObj.hasObj){
            const bType = pathObj.param.bType
            const evaType = pathObj.param.evaType.toString()
            let search = `evaType=${evaType}&bType=${bType}`

            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                search = search + `&sNum=${pathObj.param.sNum.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/brush?${search}`
        }
    })

    $('#previousPhone').click(function () {
        $('#previous').trigger('click')
    })

})