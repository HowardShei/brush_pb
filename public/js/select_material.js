const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)

$(() => {
    let mtTypeDefault = $('#displayImg').attr('data-num')
    let selectBox = [mtTypeDefault]
    const maxSelectNum = 1

    let materialList = []
    WebAPI.GetMaterialList(data => {
        if (data.errCode === 0){
            materialList = data.content
        }
    })

    $('.select-list').on('click', '.select-item', function () {
        // Prevent select item be clicked if materialList no data.
        if (materialList.length <= 0){
            return false;
        }

        const itemNum = $(this).attr('data-num')
        const bType = pathObj.param.bType[0]
        const hType = pathObj.param.hType[0]
        const selectObj = materialList.filter(function (mItem, i, array) {
            if (itemNum === mItem.code){
                return mItem
            }
        })
        // console.log(selectObj)
        let selectIndex = 0 // If isSelect is true, it can be used to store index from array.

        // Check new item exist in the array.
        let isSelected = selectBox.some(function (item, index, array) {
            selectIndex = index
            return itemNum === item
        })

        if (!isSelected) {
            // To auto select new item, and cancel first selected item.
            if (selectBox.length >= maxSelectNum){
                const firstItemNum = selectBox[0]
                $(`.select-item[data-num="${firstItemNum}"]`).removeClass('selected')
                selectBox.splice(0, 1)
            }

            $(this).addClass('selected')
            selectBox.push(itemNum)
            $('#displayImg')
                .removeClass('show')
                .attr({
                    "data-num": itemNum,
                    "style": `background: url(${selectObj[0].img_handle_brush[hType].url}) center/contain no-repeat;`
                })
            setTimeout(function () {
                $('#displayImg')
                    .addClass('show')
            }, 300)

            // set the title, description
            // title
            const title = selectObj[0].title
            $('#sTitle').html(title)
            $('#swTitle').html(title)
            $('#bsTitle').html(title)
            // description
            $('#sDes').html(selectObj[0].description)

        }
    })

    $('#next').click(function () {
        if (pathObj.hasObj){
            const currentMaterial = $('#displayImg').attr('data-num')
            const evaType = pathObj.param.evaType.toString()
            const bType = pathObj.param.bType.toString()
            const hType = pathObj.param.hType.toString()
            let currentSearch = `evaType=${evaType}&bType=${bType}&hType=${hType}&mtType=${currentMaterial}`

            if (pathObj.param.tType){
                currentSearch = currentSearch + `&tType=${pathObj.param.tType.toString()}`
            }
            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                currentSearch = currentSearch + `&sNum=${pathObj.param.sNum.toString()}`
            }
            window.location.href = `/complete/${pathObj.version}/${pathObj.mode}/topic?${currentSearch}`
        }
    })
    $('#previous').click(function () {
        if (pathObj.hasObj){
            const hType = pathObj.param.hType.toString()
            const bType = pathObj.param.bType.toString()
            const evaType = pathObj.param.evaType.toString()
            let search = `evaType=${evaType}&bType=${bType}&hType=${hType}`

            if (pathObj.param.tType){
                search = search + `&tType=${pathObj.param.tType.toString()}`
            }
            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                search = search + `&sNum=${pathObj.param.sNum.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/handle?${search}`
        }
    })

    $('#previousPhone').click(function () {
        $('#previous').trigger('click')
    })

    const $swipeList = $('#swipeList')
    const $swipeDotList = $('#swipeDotList')
    const swipeLength = $swipeList.find('.swipe-item').length
    const dotLength = $swipeDotList.find('.btn-dot').length
    const param_mtType = pathObj.param.mtType

    function showDotPrev() {
        $swipeDotList.find('.btn-dot-prev').addClass('enabled')
    }
    function hideDotPrev() {
        $swipeDotList.find('.btn-dot-prev').removeClass('enabled')
    }
    function showDotNext() {
        $swipeDotList.find('.btn-dot-next').addClass('enabled')
    }
    function hideDotNext() {
        $swipeDotList.find('.btn-dot-next').removeClass('enabled')
    }

    function checkDotNextPrev(mtType) {
        const mtType_num = parseInt(mtType.replace('mt',''))
        const isShowDotBtnPrev = mtType_num > 0
        const isShowDotBtnNext = mtType_num < (swipeLength - 1) && swipeLength > dotLength

        isShowDotBtnPrev ? showDotPrev() : hideDotPrev()
        isShowDotBtnNext ? showDotNext() : hideDotNext()
        return {
            isShowDotBtnPrev: isShowDotBtnPrev,
            isShowDotBtnNext: isShowDotBtnNext
        }
    }

    if (param_mtType){
        const mtTypeNum = param_mtType[0]
        let isSwiped = true
        $swipeList.find('.swipe-item').each(function (i, item) {
            const itemTypeNum = $(item).attr('data-num')
            if (itemTypeNum === mtTypeNum){
                $(item).addClass('selected')
                isSwiped = false
            } else if (isSwiped){
                $(item).addClass('swiped')
            }
        })

        checkDotNextPrev(mtTypeNum)
        $swipeDotList.find('.btn-dot').removeClass('selected')
        const mtType_num = parseInt(mtTypeNum.replace('mt', ''))
        if (mtType_num >= dotLength) {
            $swipeDotList.find('.btn-dot').eq((dotLength-1)).addClass('selected')
        } else {
            $swipeDotList.find('.btn-dot').eq(mtType_num).addClass('selected')
        }
    }

    const swipeNumMax = swipeLength
    const swipeNumMin = 0

    $swipeList.swipe({
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
            // console.log('event:', event)
            // console.log('direction:', direction)
            // console.log('distance:', distance)
            // console.log('duration:', duration)
            // console.log('fingerCount:', fingerCount)
            // console.log('fingerData:', fingerData)

            let currentSwipeNum = 0
            $swipeList.find('.swipe-item').each(function (i, item) {
                if ($(item).hasClass('selected')){
                    return currentSwipeNum = i
                }
            })

            if (direction === 'left'){
                if ((currentSwipeNum + 1) >= swipeNumMax) return false;
                $swipeList.find('.swipe-item').eq(currentSwipeNum).addClass('swiped').removeClass('selected')
                $swipeList.find('.swipe-item').eq((currentSwipeNum+1)).addClass('selected')
                currentSwipeNum++
                // console.error('[left End]\n currentSwipeNum:', currentSwipeNum)
            }
            if (direction === 'right'){
                if (currentSwipeNum <= swipeNumMin) return false;
                $swipeList.find('.swipe-item').eq(currentSwipeNum).removeClass('swiped').removeClass('selected')
                $swipeList.find('.swipe-item').eq((currentSwipeNum-1)).removeClass('swiped').addClass('selected')
                currentSwipeNum--
                // console.error('[right End]\n currentSwipeNum:', currentSwipeNum)
            }

            // Dot
            const mtType = `mt${currentSwipeNum}`
            checkDotNextPrev(mtType)
            let lastDotNum = 0
            $swipeDotList.find('.btn-dot').each(function (i, item) {
                if ($(item).hasClass('selected')) return lastDotNum = i
            })
            let isLastDotNumChange = lastDotNum < (dotLength-1) || lastDotNum > 0
            if (direction === 'left' && lastDotNum < (dotLength-1)) {
                lastDotNum++
            }
            if (direction === 'right' && lastDotNum > 0) {
                lastDotNum--
            }
            if (isLastDotNumChange){
                $swipeDotList.find('.btn-dot').removeClass('selected')
                $swipeDotList.find('.btn-dot').eq(lastDotNum).addClass('selected')
            }


            // set the title, description
            const itemNum = $swipeList.find('.swipe-item').eq(currentSwipeNum).attr('data-num')
            const selectObj = materialList.filter(function (mItem, i, array) {
                if (itemNum === mItem.code){
                    return mItem
                }
            })
            $swipeList.attr('data-num', itemNum)

            // title
            const title = selectObj[0].title
            $('#sTitle').html(title)
            $('#swTitle').html(title)
            $('#bsTitle').html(title)
            // description
            $('#sDes').html(selectObj[0].description)
        },
        allowPageScroll: "vertical"
    })

    $swipeList.on('click', '.swipe-item', function () {

        $swipeList.toggleClass('selected')

        const style = $(this).attr('style')
        const dataNum = $(this).attr('data-num')
        $('#displayImg').attr({
            'data-num': dataNum,
            'style': style
        })

        // Use to portrait version.
        if ($swipeList.hasClass('selected')) {
            $('.block-bottom').addClass('active')
        } else {
            $('.block-bottom').removeClass('active')
        }
    })
})