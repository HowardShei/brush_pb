const WebAPI = {
    TAG: "WebAPI.js",
    APP_TYPE: "WEB",

    // Get Brush list
    GetBrushList: function (callback) {
        const api = '/GetBrushList'
        const methodType = 'POST'
        const headers = {}
        const body = {}
        const elseOpts = {
            apiName: arguments.callee.name
        }

        this.AjaxCall((this.URL + api), methodType, headers, body, callback, elseOpts)
    },
    // Get Handle list
    GetHandleList: function (callback) {
        const api = '/GetHandleList'
        const methodType = 'POST'
        const headers = {}
        const body = {}
        const elseOpts = {
            apiName: arguments.callee.name
        }

        this.AjaxCall((this.URL + api), methodType, headers, body, callback, elseOpts)
    },
    // Get Material list
    GetMaterialList: function (callback) {
        const api = '/GetMaterialList'
        const methodType = 'POST'
        const headers = {}
        const body = {}
        const elseOpts = {
            apiName: arguments.callee.name
        }

        this.AjaxCall((this.URL + api), methodType, headers, body, callback, elseOpts)
    },
    // Check if reply the Surveycake
    CheckSurveycakeEnd: function (serial_number, callback) {
        const api = '/CheckSurveycakeEnd'
        const methodType = 'POST'
        const headers = {}
        const body = {
            serial_number: serial_number
        }
        const elseOpts = {
            apiName: arguments.callee.name
        }

        this.AjaxCall((this.URL + api), methodType, headers, body, callback, elseOpts)
    },

    AjaxCall: function (url, methodType, headers, body, callback, elseOpts) {
        let apiCallForm = "application/json; charset=utf-8"
        const apiName = elseOpts.apiName
        const processData = true

        switch (methodType){
            case "GET":
                break;
            case "POST":
                body = JSON.stringify(body)
                break;
        }

        $.ajax({
            url: url,
            type: methodType,
            contentType: apiCallForm,
            processData: processData,
            headers: headers,
            data: body,
            dataType: 'json',
            success: function (data) {
                // console.log(`[API Success] ${apiName}`)
                // console.log(data)
                callback(data)
            },
            error: function (data) {
                // console.error(`[API Error] ${apiName}`)
                // console.error(`ajax call error: ${data.errorMsg}`)
                callback(data)
            }
        })
    }
}