本專案所使用的資源如下：

=========================

開發環境與框架：
1. Node.js v10.13.0
2. Express framework

前端開發的模板與套件：
1. Jade
2. Sass
3. Gulp
4. Bower

=========================

架設的環境與服務：
1. Google Cloud Platform - App Engine
2. Google Cloud Platform - Storage
3. Google Cloud Platform - Datastore

=========================