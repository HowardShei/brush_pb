let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');

const index = require('./routes/index');
const select = require('./routes/select');
const complete = require('./routes/complete');
const api = require('./routes/api');
const webhook = require('./routes/webhook');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/home/normal', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/home/business', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/select/normal', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/select/business', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/select/normal/feature', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/select/normal/makeup', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/select/business/feature', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/select/business/makeup', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/complete/normal/feature', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/complete/normal/makeup', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/complete/business/feature', express.static(path.join(__dirname, 'public'), {redirect: false}));
app.use('/complete/business/makeup', express.static(path.join(__dirname, 'public'), {redirect: false}));

app.use('/', index);
app.use('/select', select);
app.use('/complete', complete);
app.use('/API', api);
app.use('/webhook', webhook);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  console.error("error handler")
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  if (err.status === 404){
    let pathParam = req.path.split('/')
    let website_version = pathParam[2]
    let website_mode = pathParam[3]
    let redirectAddress = '/'

    console.error('website_version:', website_version)
    if (website_version !== 'n' && website_version !== 'b'){
        console.error('[404] Can\'t get the website version.')
        res.redirect(redirectAddress)
        return err.status
    }

    switch (website_version){
        case "n":
            redirectAddress = '/home/normal'
            if (website_mode === 'f') redirectAddress = redirectAddress + '/feature'
            if (website_mode === 'm') redirectAddress = redirectAddress + '/makeup'
          break;
        case "b":
            redirectAddress = '/home/business'
            if (website_mode === 'f') redirectAddress = redirectAddress + '/feature'
            if (website_mode === 'm') redirectAddress = redirectAddress + '/makeup'
            break;
    }
    console.error('redirectAddress:', redirectAddress)
    res.redirect(redirectAddress)
    return err.status;
  }

  // render the error page
  res.status(err.status || 500);
  res.end();
  // res.render('error');
});

module.exports = app;
