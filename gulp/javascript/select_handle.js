const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)

$(() => {
    let hTypeDefault = $('#displayImg').attr('data-num')
    let selectBox = [hTypeDefault]
    const maxSelectNum = 1

    let handleList = []
    WebAPI.GetHandleList(data => {
        if (data.errCode === 0){
            handleList = data.content
        }
    })

    $('.select-list').on('click', '.select-item', function () {
        // Prevent select item be clicked if handleList no data.
        if (handleList.length <= 0){
            return false;
        }

        const itemNum = $(this).attr('data-num')
        const itemName = $(this).attr('data-name')
        const brushFile = $(this).attr('data-brushFile')
        let selectIndex = 0 // If isSelect is true, it can be used to store index from array.

        // Check new item exist in the array.
        let isSelected = selectBox.some(function (item, index, array) {
            selectIndex = index
            return itemNum === item
        })

        if (!isSelected) {
            // To auto select new item, and cancel first selected item.
            if (selectBox.length >= maxSelectNum){
                const firstItemNum = selectBox[0]
                $(`.select-item[data-num="${firstItemNum}"]`).removeClass('selected')
                selectBox.splice(0, 1)
            }

            $(this).addClass('selected')
            selectBox.push(itemNum)
            $('#displayImg')
                .removeClass('show')
                .attr({
                    "data-num": itemNum,
                    "style": `background: url("https://storage.googleapis.com/brush-by-you.appspot.com/handle/${itemName}.png") center/contain no-repeat;`
                })
                .find('.select-brush-display')
                .attr('style', `background: url("https://storage.googleapis.com/brush-by-you.appspot.com/handle_brush/${brushFile}") center/contain no-repeat;`)
            setTimeout(function () {
                $('#displayImg')
                    .addClass('show')
            }, 300)

            // set the title, description, eva
            handleList.some(function (handle, i, array) {
                if (itemNum === handle.code){
                    // title
                    const title = handle.title
                    $('#sTitle').html(title)
                    $('#swTitle').html(title)
                    $('#bsTitle').html(title)
                    // description
                    $('#sDes').html(handle.description)
                    // eva
                    const $sEva = $('#sEva')
                    switch (pathObj.mode){
                        case "feature":
                            $sEva.find('.trait-star-list li').each(function (i, li) {
                                const tType = $(li).find('.trait-type').attr('data-type')
                                $(li).find('.eva-star-list').attr('data-evanum', handle.eva_trait[tType])
                            })
                            break;
                        case "makeup":
                            const bType = pathObj.param.bType[0]
                            $sEva.find('.makeup-type').html(handle.eva_brush_people[bType].name)
                            $sEva.find('.people-num').html(handle.eva_brush_people[bType].num)
                            break;
                    }
                    return true;
                }
            })
        }
    })

    $('#next').click(function () {
        if (pathObj.hasObj){
            const currentHandle = $('#displayImg').attr('data-num')
            const evaType = pathObj.param.evaType.toString()
            const bType = pathObj.param.bType.toString()
            let currentSearch = `evaType=${evaType}&bType=${bType}&hType=${currentHandle}`

            if (pathObj.param.tType){
                currentSearch = currentSearch + `&tType=${pathObj.param.tType.toString()}`
            }
            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                currentSearch = currentSearch + `&sNum=${pathObj.param.sNum.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/material?${currentSearch}`
        }
    })
    $('#previous').click(function () {
        if (pathObj.hasObj){
            const bType = pathObj.param.bType
            const evaType = pathObj.param.evaType.toString()
            let search = `evaType=${evaType}&bType=${bType}`
            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                search = search + `&sNum=${pathObj.param.sNum.toString()}`
            }
            if (pathObj.mode === 'makeup'){
                return window.location.href = `/select/${pathObj.version}/${pathObj.mode}/brush?${search}`
            }
            if (pathObj.param.tType){
                search = search + `&tType=${pathObj.param.tType.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/trait?${search}`
        }
    })

    $('#previousPhone').click(function () {
        $('#previous').trigger('click')
    })

    const $swipeList = $('#swipeList')
    const $swipeDotList = $('#swipeDotList')

    let swipeNav = SwipeNav($swipeList, $swipeDotList)

    const param_hType = pathObj.param.hType
    if (param_hType){
        swipeNav.setScrollImgNum(param_hType[0].replace('h', ''), function (isSetScroll) {
            // console.error('isSetScroll: ', isSetScroll)
        })
    }

    $swipeList.swipe({
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
            // console.log('event:', event)
            // console.log('direction:', direction)
            // console.log('distance:', distance)
            // console.log('duration:', duration)
            // console.log('fingerCount:', fingerCount)
            // console.log('fingerData:', fingerData)

            if (direction !== 'left' && direction !== 'right') return false;

            const swipeItemWidth = $(this).find('.swipe-item').width()
            // console.log('swipeItemWidth:', swipeItemWidth)

            let currentScroll = $(this).scrollLeft()
            // console.log('currentScroll:', currentScroll)

            if (direction === 'left'){
                swipeNav.setImgNext()
            }
            if (direction === 'right'){
                swipeNav.setImgPrev()
            }
            swipeNav.reloadSwipeNav()
            swipeNav.transformInterfaceValue(dataNum =>{
                if (handleList.length <= 0) {
                    return false;
                }
                // set the title, description, eva
                handleList.some(function (handle, i, array) {
                    if (dataNum === handle.code){
                        // title
                        const title = handle.title
                        $('#sTitle').html(title)
                        $('#swTitle').html(title)
                        $('#bsTitle').html(title)
                        // description
                        $('#sDes').html(handle.description)
                        // eva
                        const $sEva = $('#sEva')
                        switch (pathObj.mode){
                            case "feature":
                                $sEva.find('.trait-star-list li').each(function (i, li) {
                                    const tType = $(li).find('.trait-type').attr('data-type')
                                    $(li).find('.eva-star-list').attr('data-evanum', handle.eva_trait[tType])
                                })
                                break;
                            case "makeup":
                                const bType = pathObj.param.bType[0]
                                $sEva.find('.makeup-type').html(handle.eva_brush_people[bType].name)
                                $sEva.find('.people-num').html(handle.eva_brush_people[bType].num)
                                break;
                        }
                        return true;
                    }
                })

            })
        },
        allowPageScroll: "vertical"
    })

    $swipeList.on('click', '.swipe-item', function () {

        $swipeList.toggleClass('selected')

        const style = $(this).attr('style')
        const dataNum = $(this).attr('data-num')
        $('#displayImg').attr({
            'data-num': dataNum,
            'style': style
        })

        // Use to portrait version.
        if ($swipeList.hasClass('selected')) {
            $('.block-bottom').addClass('active')
        } else {
            $('.block-bottom').removeClass('active')
        }
    })
})