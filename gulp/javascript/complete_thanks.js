const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)
$(() => {
    let tUrl = $('.complete-img-puzzle').find('.puzzle-img').eq(0).attr('style')
    tUrl = tUrl.split('(')[1].split(')')[0]

    const tImg = new Image()
    tImg.onload = function () {
        $('.page-complete-thanks').addClass('show-animation').removeClass('before-animation')

        setTimeout(function () {
            $('.page-complete-thanks').removeClass('show-animation')
        }, 3000)
    }
    tImg.src = tUrl
})