const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)

$(() => {
    let bTypeDefault = $('#displayImg').attr('data-num')
    let selectBox = [bTypeDefault]
    const maxSelectNum = 1

    let brushList = []
    WebAPI.GetBrushList(data => {
        if (data.errCode === 0){
            brushList = data.content
        }
    })

    $('.select-list').on('click', '.select-item', function () {
        // Prevent select item be clicked if brushList no data.
        if (brushList.length <= 0){
            return false;
        }

        const itemNum = $(this).attr('data-num')
        const itemName = $(this).attr('data-name')
        let selectIndex = 0 // If isSelect is true, it can be used to store index from array.

        // Check new item exist in the array.
        let isSelected = selectBox.some(function (item, index, array) {
            selectIndex = index
            return itemNum === item
        })

        if (!isSelected) {
            // To auto select new item, and cancel first selected item.
            if (selectBox.length >= maxSelectNum){
                const firstItemNum = selectBox[0]
                $(`.select-item[data-num="${firstItemNum}"]`).removeClass('selected')
                selectBox.splice(0, 1)
            }

            $(this).addClass('selected')
            selectBox.push(itemNum)
            $('#displayImg')
                .removeClass('show')
                .attr({
                    "data-num": itemNum,
                    "style": `background: url("https://storage.googleapis.com/brush-by-you.appspot.com/brush/${itemName}.png") center/contain no-repeat;`
                })
            setTimeout(function () {
                $('#displayImg')
                    .addClass('show')
            }, 300)

            // set the title, description, eva
            brushList.some(function (brush, i, array) {
                if (itemNum === brush.code){
                    // title
                    const title = brush.title
                    $('#sTitle').html(title)
                    $('#swTitle').html(title)
                    $('#bsTitle').html(title)
                    // description
                    $('#sDes').html(brush.description)
                    // eva
                    switch (pathObj.mode){
                        case "feature":
                            $('.feature-star-list li').each(function (i, li) {
                                const fType = $(li).find('.feature-type').attr('data-type')
                                $(li).find('.eva-star-list').attr('data-evanum', brush.eva_feature[fType])
                            })
                            break;
                        case "makeup":
                            const $makeupCount = $('.makeup-count-subtitle')
                            const mType = $makeupCount.attr('data-type')
                            $makeupCount.find('.people-num').html(brush.eva_makeup[mType].num)
                            break;
                    }
                    return true;
                }
            })
        }
    })

    $('#next').click(function () {
        if (pathObj.hasObj){
            const currentBrush = $('#displayImg').attr('data-num')
            let currentSearch = `evaType=${pathObj.param.evaType.toString()}&bType=${currentBrush}`

            // If serial_number has been exist, which take it to next page.
            if (pathObj.param.sNum){
                currentSearch = currentSearch + `&sNum=${pathObj.param.sNum.toString()}`
            }
            window.location.href = `/select/${pathObj.version}/${pathObj.mode}/trait?${currentSearch}`
        }
    })

    $('#previous').click(function () {
        const evaType = pathObj.param.evaType.toString()
        let search = `evaType=${evaType}&noNews=true`
        // If serial_number has been exist, which take it to next page.
        if (pathObj.param.sNum){
            search = search + `&sNum=${pathObj.param.sNum.toString()}`
        }
        window.location.href = `/select/${pathObj.version}/${pathObj.mode}?${search}`
    })

    $('#previousPhone').click(function () {
        $('#previous').trigger('click')
    })

    const $swipeList = $('#swipeList')
    const $swipeDotList = $('#swipeDotList')

    let swipeNav = SwipeNav($swipeList, $swipeDotList)

    const param_bType = pathObj.param.bType
    if (param_bType){
        swipeNav.setScrollImgNum(param_bType[0].replace('b', ''), function (isSetScroll) {
            // console.error('isSetScroll: ', isSetScroll)
        })
    }

    $swipeList.swipe({
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
            // console.log('event:', event)
            // console.log('direction:', direction)
            // console.log('distance:', distance)
            // console.log('duration:', duration)
            // console.log('fingerCount:', fingerCount)
            // console.log('fingerData:', fingerData)

            if (direction !== 'left' && direction !== 'right') return false;

            const swipeItemWidth = $(this).find('.swipe-item').width()
            // console.log('swipeItemWidth:', swipeItemWidth)

            let currentScroll = $(this).scrollLeft()
            // console.log('currentScroll:', currentScroll)

            if (direction === 'left'){
                swipeNav.setImgNext()
            }
            if (direction === 'right'){
                swipeNav.setImgPrev()
            }
            swipeNav.reloadSwipeNav()
            swipeNav.transformInterfaceValue(dataNum =>{
                if (brushList.length <= 0) {
                    return false;
                }
                // set the title, description, eva
                brushList.some(function (brush, i, array) {
                    if (dataNum === brush.code){
                        // title
                        const title = brush.title
                        $('#sTitle').html(title)
                        $('#swTitle').html(title)
                        $('#bsTitle').html(title)
                        // description
                        $('#sDes').html(brush.description)
                        // eva
                        switch (pathObj.mode){
                            case "feature":
                                $('.feature-star-list li').each(function (i, li) {
                                    const fType = $(li).find('.feature-type').attr('data-type')
                                    $(li).find('.eva-star-list').attr('data-evanum', brush.eva_feature[fType])
                                })
                                break;
                            case "makeup":
                                const $makeupCount = $('.makeup-count-subtitle')
                                const mType = $makeupCount.attr('data-type')
                                $makeupCount.find('.people-num').html(brush.eva_makeup[mType].num)
                                break;
                        }
                        return true;
                    }
                })

            })
        },
        allowPageScroll: "vertical"
    })


    $swipeList.on('click', '.swipe-item', function () {

        $swipeList.toggleClass('selected')

        const style = $(this).attr('style')
        const dataNum = $(this).attr('data-num')
        $('#displayImg').attr({
            'data-num': dataNum,
            'style': style
        })

        // Use to portrait version.
        if ($swipeList.hasClass('selected')) {
            $('.block-bottom').addClass('active')
        } else {
            $('.block-bottom').removeClass('active')
        }
    })

})