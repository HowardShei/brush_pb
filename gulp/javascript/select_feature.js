const pathObj = getPathObject()
// console.log(pathObj)
CamouflageUrl(pathObj)

$(() => {
    const newsDialog = CustomDialog('news')
    if (pathObj.param.noNews){
        if (pathObj.param.noNews[0] !== 'true') newsDialog.showDialog()
    } else {
        newsDialog.showDialog()
    }

    let selectBox = []
    const maxSelectNum = 2

    function CheckShowBottomBar() {
        // Use in portrait version.
        if (selectBox.length >= maxSelectNum) {
            $('.block-bottom').addClass('active')
        } else {
            $('.block-bottom').removeClass('active')
        }
    }

    // Set the select-item selected if search exist.
    if (pathObj.param.hasOwnProperty("evaType")){
        pathObj.param.evaType.forEach(function (type, i, array) {
            $('.select-container').find('.select-item').each(function (i, item) {
                if ($(item).attr('data-num') === type){
                    $(item).addClass('selected')
                    selectBox.push(type)
                }
            })
        })
    }
    CheckShowBottomBar()

    $('.select-item').click(function () {
        const itemNum = $(this).attr('data-num')
        let selectIndex = 0 // If isSelect is true, it can be used to store index from array.
        // console.log('itemNum: ', itemNum)

        // Check new item exist in the array.
        let isSelected = selectBox.some(function (item, index, array) {
            selectIndex = index
            return itemNum === item
        })

        if (isSelected){
            // To auto cancel selected item.
            $(this).removeClass('selected')
            selectBox.splice(selectIndex, 1)
        } else {
            // To auto select new item, and cancel first selected item.
            if (selectBox.length >= maxSelectNum){
                const firstItemNum = selectBox[0]
                $(`.select-item[data-num="${firstItemNum}"]`).removeClass('selected')
                selectBox.splice(0, 1)
            }

            $(this).addClass('selected')
            selectBox.push(itemNum)
        }

        CheckShowBottomBar()
    })

    $('#next').click(function () {
        if (selectBox.length < maxSelectNum){
            errMsgDialog.editInfo('你需要選擇兩個粉底刷的特色')
            errMsgDialog.showDialog()
            return false;
        }

        // console.log(selectBox.toString())
        const selectedParam = selectBox.toString()
        window.location.href = `/select/${pathObj.version}/feature/brush?evaType=${selectedParam}`
    })

})