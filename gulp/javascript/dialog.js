function CustomDialog(type, info) {
    if (!(this instanceof CustomDialog)) return new CustomDialog(type, info)

    this.type = type
    this.info = info
    this.dialogID = ''

    switch (type) {
        case "errMsg":
            this.dialogID = 'errMsgDialog'
            break;
        case "news":
            this.dialogID = 'newsDialog'
            break;
    }
}
(function () {
    this.editInfo = function (info) {
        this.info = info
        this.refreshInfo()
    }
    this.showDialog = function () {
        const cDialog = this
        const dialogID = cDialog.dialogID

        if (this.type === 'news') {
            $(`#${dialogID}`).addClass('show').addClass('visible')
            $('body').addClass('dialog-news-show')
            setTimeout(function () {
                $(`#${dialogID}`).addClass('fadeIn-content')
            }, 500)
            setTimeout(function () {
                $(`#${dialogID}`).addClass('fadeout')
                $('body').addClass('footer-fadeout')
            }, 2500)
            setTimeout(function () {
                cDialog.hideDialog()
            }, 3000)
            return true;
        }

        if (this.type === 'errMsg') {
            $(`#${dialogID}`).addClass('show')
            $('body').addClass('dialog-show')
            setTimeout(function () {
                $(`#${dialogID}`).addClass('fadeIn')
            }, 100)
            return true;
        }
    }

    this.hideDialog = function () {

        if (this.type === 'news'){
            $(`#${this.dialogID}`).removeClass('show').removeClass('visible').removeClass('fadeout')
            $('body').removeClass('dialog-news-show').removeClass('footer-fadeout')
        }

        if (this.type === 'errMsg'){
            $(`#${this.dialogID}`).removeClass('show').removeClass('fadeIn')
            $('body').removeClass('dialog-show')
            return true;
        }
    }

    this.refreshInfo = function () {
        switch (this.type){
            case "errMsg":
                $(`#${this.dialogID}`).find('.dialog-info').html(this.info)
                break;
        }
    }

    this.registerEvent_errMsg = function () {
        const cDialog = this
        $('#errMsgDialog').on('click', function (e) {
            const $target = $(e.target)

            if ($target.hasClass('dialog-container') || $target.hasClass('btn-dialog-confirm')){
                return cDialog.hideDialog()
            }
            return false;
        })
    }

    this.init = function () {
        switch (this.type){
            case "errMsg":
                this.registerEvent_errMsg()
                break;
        }
    }
}).call(CustomDialog.prototype)

const errMsgDialog = CustomDialog('errMsg')
errMsgDialog.init()