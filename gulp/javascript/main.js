function getPathObject() {
    let path = window.location.pathname
    let search = window.location.search ? window.location.search : ""
    let param = {}

    if (path === '' || path === null){
        return {hasObj: false}
    }

    let pArray = path.split('/')

    if (search !== ''){
        let s = search.replace('?', '')
        if (s.indexOf('&') >= 0){
            s = s.split('&')
            s.forEach(function (val, i, array) {
                let p = val.split('=')
                param[p[0]] = p[1].indexOf(',') ? p[1].split(',') : p[1]
            })
        } else {
            let p = s.split('=')
            param[p[0]] = p[1].indexOf(',') ? p[1].split(',') : p[1]
        }
    }

    return {
        hasObj: true,
        pathArray: pArray,
        category: pArray[1],
        version: pArray[2],
        mode: pArray[3],
        search: search,
        param: param
    }
}

function setSwipeDot(targetDot) {
    const $swDotList = $('.swipe-dots-list')
    const maxDots = parseInt($swDotList.attr('data-max'))

    if (maxDots > 6) {
        $swDotList.find('.')
    }
}

function SwipeNav($swipeImgList, $swipeDotList) {
    if (!(this instanceof SwipeNav)) return new SwipeNav($swipeImgList, $swipeDotList)
    this.$swipeImgList = $swipeImgList
    this.$swipeDotList = $swipeDotList
    this.imgTotal = $swipeImgList.find('.swipe-item').length
    this.imgWidth = $swipeImgList.find('.swipe-item').width()
    this.dotTotal = $swipeDotList.find('.btn-dot').length
    this.currentImgNum = 0
    this.currentDotNum = 0
    this.currentScroll = 0
    this.swipeSpeed = 150
    this.swipeDirection = 'next'    // next => direction == 'left'; prev => direction == 'right';
    this.dataNum = $swipeImgList.find('.swipe-item').eq(0).attr('data-num')
    this.dataBrushFile = $swipeImgList.find('.swipe-item').eq(0).attr('data-brushFile') ? $swipeImgList.find('.swipe-item').eq(0).attr('data-brushFile') : 'none'
}
(function () {
    this.setImgNext = function () {
        if (this.currentImgNum + 1 >= this.imgTotal) return false;
        this.currentImgNum++
        this.swipeDirection = 'next'
    }
    this.setImgPrev = function () {
        if (this.currentImgNum <= 0) return false;
        this.currentImgNum--
        this.swipeDirection = 'prev'
    }
    this.setCurrentImgNum = function (num) {
        this.currentImgNum = num
    }
    this.setDataNum = function (dataNum) {
        if (dataNum) return this.dataNum = dataNum
        this.dataNum = this.$swipeImgList.find('.swipe-item').eq(this.currentImgNum).attr('data-num')
    }
    this.getDataNum = function () {
        return this.dataNum
    }
    this.setDataBrushFile = function (dataBrushFile) {
        if (dataBrushFile) return this.dataBrushFile = dataBrushFile
        this.dataBrushFile = this.$swipeImgList.find('.swipe-item').eq(this.currentImgNum).attr('data-brushFile')
    }
    this.getDataBrushFile = function () {
        return this.dataBrushFile
    }
    this.showSwipeBrush = function () {
        this.$swipeImgList.nextAll('.swipe-select-brush').addClass('show')
    }
    this.hideSwipeBrush = function () {
        this.$swipeImgList.nextAll('.swipe-select-brush').removeClass('show')
    }
    this.showDotBtnPrev = function () {
        this.$swipeDotList.find('.btn-dot-prev').addClass('enabled')
    }
    this.showDotBtnNext = function () {
        this.$swipeDotList.find('.btn-dot-next').addClass('enabled')
    }
    this.hideDotBtnPrev = function () {
        this.$swipeDotList.find('.btn-dot-prev').removeClass('enabled')
    }
    this.hideDotBtnNext = function () {
        this.$swipeDotList.find('.btn-dot-next').removeClass('enabled')
    }
    // Use in setting and checking the current scroll, if it be more or less then min scroll value or max scroll value.
    this.setCurrentScroll = function (scroll) {
        if (scroll < 0 || scroll >= this.imgTotal * this.imgWidth) {
            // console.error('newScroll: ', scroll, ' is error. \n SwipeItemMaxWidth: ', this.imgTotal * this.imgWidth)
            return false;
        }
        this.currentScroll = scroll
        return true;
    }
    this.setSwipeDot = function () {
        this.checkDotNextPrev()

        this.$swipeDotList.find('.btn-dot').removeClass('selected')
        if (this.currentImgNum >= this.dotTotal) {
            this.$swipeDotList.find('.btn-dot').eq((this.dotTotal-1)).addClass('selected')
        } else {
            this.$swipeDotList.find('.btn-dot').eq(this.currentImgNum).addClass('selected')
        }
    }
    this.setScrollImgNum = function (imgNum, callback) {
        this.currentImgNum = imgNum - 1     // Default the first number is 0.
        const newScroll = this.currentImgNum * this.imgWidth
        const isSetScroll = this.setCurrentScroll(newScroll)

        this.setSwipeDot()

        if (isSetScroll){
            this.$swipeImgList.animate({
                scrollLeft: newScroll
            }, this.swipeSpeed, callback(isSetScroll))
        } else {
            callback(isSetScroll)
        }
    }
    this.scrollSwipeImg = function (callback) {
        // const oriScroll = this.currentScroll
        const newScroll = this.currentImgNum * this.imgWidth
        const isSetScroll = this.setCurrentScroll(newScroll)

        if (isSetScroll){
            if (this.dataBrushFile !== 'none') this.hideSwipeBrush()
            this.$swipeImgList.animate({
                scrollLeft: newScroll
            }, this.swipeSpeed, callback(isSetScroll))
        } else {
            callback(isSetScroll)
        }

    }
    this.reselectSwipeImg = function () {
        const currentImgNum = this.currentImgNum
        this.$swipeImgList.find('.swipe-item').each(function (i, item) {
            if ($(item).hasClass('selected')){
                $(item).removeClass('selected')
            }
            if (i === currentImgNum) {
                $(item).addClass('selected')
            }
        })
    }
    this.reselectSwipeBrush = function () {
        const style = `background: url("https://storage.googleapis.com/brush-by-you.appspot.com/handle_brush/${this.dataBrushFile}") center/contain no-repeat;`
        this.$swipeImgList.nextAll('.swipe-select-brush').attr('style', style)
    }
    this.checkDotNextPrev = function () {
        const isShowDotBtnPrev = this.currentImgNum > 0
        const isShowDotBtnNext = this.currentImgNum < (this.imgTotal - 1) && this.imgTotal > this.dotTotal

        isShowDotBtnPrev ? this.showDotBtnPrev() : this.hideDotBtnPrev()
        isShowDotBtnNext ? this.showDotBtnNext() : this.hideDotBtnNext()
        return {
            isShowDotBtnPrev: isShowDotBtnPrev,
            isShowDotBtnNext: isShowDotBtnNext
        }
    }
    this.reselectSwipeDot = function () {
        const currentImgNum = this.currentImgNum
        const dotLengthMax = this.dotTotal
        const swipeDirection = this.swipeDirection

        // console.error('currentImgNum: ',currentImgNum)
        // console.error('this.imgTotal: ',this.imgTotal)

        this.checkDotNextPrev()

        let lastDotNum = 0
        this.$swipeDotList.find('.btn-dot').each(function (i, item) {
            if ($(item).hasClass('selected')) return lastDotNum = i
        })
        let isLastDotNumChange = lastDotNum < (dotLengthMax-1) || lastDotNum > 0
        if (swipeDirection === 'next' && lastDotNum < (dotLengthMax-1)) {
            lastDotNum++
        }
        if (swipeDirection === 'prev' && lastDotNum > 0) {
            lastDotNum--
        }
        if (isLastDotNumChange){
            this.$swipeDotList.find('.btn-dot').removeClass('selected')
            this.$swipeDotList.find('.btn-dot').eq(lastDotNum).addClass('selected')
        }
    }
    this.reloadSwipeNav = function () {
        const swipeNav = this

        swipeNav.scrollSwipeImg(function (isSetSuccess) {
            if (isSetSuccess){
                swipeNav.reselectSwipeImg()
                swipeNav.reselectSwipeDot()
                swipeNav.setDataNum()
                if (swipeNav.dataBrushFile !== 'none') {
                    swipeNav.setDataBrushFile()
                    swipeNav.reselectSwipeBrush()
                    setTimeout(function () {
                        swipeNav.showSwipeBrush()
                    }, 300)
                }
                // console.log('==== swipe success ====')
            }
        })
    }
    this.transformInterfaceValue = function (callback) {
        callback(this.dataNum)
    }
    
}).call(SwipeNav.prototype)

function getProduct(bType, hType, mtType) {
    let brushNum = bType.toUpperCase()
    let handleNum = hType.toUpperCase()
    let materialNum = mtType.replace('t', "").toUpperCase()
    let hChars = handleNum.split("")
    let mtChars = materialNum.split("")

    if (hChars.length <= 2){
        handleNum = `${hChars[0]}0${hChars[1]}`
    }
    if (mtChars.length <= 2){
        materialNum = `${mtChars[0]}0${mtChars[1]}`
    }
    let fileName = `${brushNum}_${materialNum}${handleNum}`

    return {
        fileName: fileName,
        search: `?product=${fileName}`
    }
}

function CamouflageUrl(pathObj) {
    let cUrl = ''

    pathObj.pathArray.forEach(function (val, i, array) {
        if (i === 0) return false;
        if (i === (pathObj.pathArray.length-1)){
            cUrl = cUrl + '/' + val
            if (val === 'topic') {
                if (pathObj.param.bType && pathObj.param.hType && pathObj.param.mtType){
                    const product = getProduct(pathObj.param.bType[0], pathObj.param.hType[0], pathObj.param.mtType[0])
                    cUrl = cUrl + product.search
                }
            }
            if (val === 'thanks') cUrl = cUrl + pathObj.search
            return false;
        }
        cUrl = cUrl + '/' + val.substr(0, 1).toLowerCase()
    })

    let stateObj = {}
    history.replaceState(stateObj, 'page', cUrl)
}

$(() => {
    $('a').on('click', function (e) {

        if (this.hash.charAt(0) !== "#"){
            return;
        }

        if (this.hash !== "" || this.hash !== "#" || this.hash !== "#0"){
            // Prevent default anchor click behavior
            e.preventDefault()

            const hash = this.hash

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, () => {
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash
            })
        }
    })
})