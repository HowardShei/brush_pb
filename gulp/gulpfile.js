const gulp = require('gulp')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const rename = require('gulp-rename')
const sass = require('gulp-ruby-sass')
const autoprefixer = require('gulp-autoprefixer')
const browserSync = require('browser-sync').create()
const jade = require('gulp-jade')

const PUBLIC = '../public'
const MAIN_JS = `${PUBLIC}/js`
const MAIN_CSS = `${PUBLIC}/css`

/**
 * Front desk.
 * */
const compileSASS = (filename, option) => {
    return sass('scss/*.scss', option)
        .pipe(autoprefixer('last 2 version', '> 5%'))
        .pipe(concat(filename))
        .pipe(gulp.dest(MAIN_CSS))
        .pipe(browserSync.stream())
}

gulp.task('scripts', () => {
    return gulp
        .src(['javascript/*.js', 'javascript/**/*.js'])
        .pipe(gulp.dest(MAIN_JS))
        .pipe(browserSync.stream())
})

gulp.task('sass', () => {
    return compileSASS('custom-css.css', {})
})

gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: '../'
        },
        startPath: '../public/index.html'
    })
})

gulp.task('watch', () => {
    // Watch .jade file
    // gulp.watch('./../views/**/*.jade', ['template'])

    // Watch .js file
    gulp.watch('./javascript/*.js', ['scripts'])

    // Watch .scss file
    gulp.watch('./scss/**/*.scss', ['sass'])
})

gulp.task('template', () => {
    const JADE_PATH = ['../views/*.jade']

    gulp.src(JADE_PATH)
        .pipe(jade({
            locals: {}
        }))
        // .pipe(gulp.dest('../public/template'))
})

gulp.task('default', ['watch'])